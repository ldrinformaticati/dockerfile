FROM ubuntu:trusty

LABEL MAINTAINER="Leandro Oliveira"
LABEL APP_VERSION="1.0.0.0"

ENV NPM_VERSION=8 ENVIRONTMENT=PROD

RUN apt-get update && apt-get install -y git nano npm

WORKDIR /usr/share/myapp

COPY . .

ENTRYPOINT [ "ping" ]

CMD [ "localhost" ]